package System;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import Interface.Measurable;
import Interface.Taxable;
import Model.Company;
import Model.Country;
import Model.EarningComparator;
import Model.ExpenseComparator;
import Model.Person;
import Model.Product;
import Model.ProfitComparator;
import Model.TaxCalculator;

public class Tester {

	public static void main(String[] args) {
		Tester tester = new Tester();
		tester.test_Person();
		tester.test_Product();
		tester.test_Company();
		tester.test_Tax();
	}

	private void test_Tax() {
		ArrayList<Object> taxobj = new ArrayList <Object>();
		taxobj.add(new Person("John", 185, 158900));
		taxobj.add(new Product("Mazda3 Skyactive", 380000));
		taxobj.add(new Company("CP Company", 1800000, 500000));
		
	}

	public void test_Person() {

		ArrayList<Person> personlist = new ArrayList<Person>();
		personlist.add(new Person("Nannia ribena", 145, 120406));
		personlist.add(new Person("Way Tita", 180, 165000));
		personlist.add(new Person("Lion", 175, 1200));
		Collections.sort(personlist);

		Iterator itr = personlist.iterator();

		while (itr.hasNext()) {
			Object element = itr.next();
			System.out.println(element + "\n");

		}
		System.out.println("##############################################");
	}

	public void test_Product() {
		ArrayList<Product> pr = new ArrayList<Product>();
		pr.add(new Product("Kit Kat", 200));
		pr.add(new Product("Farm House", 250));
		pr.add(new Product("Harrod Bag", 2800));
		Collections.sort(pr);

		Iterator itr = pr.iterator();

		while (itr.hasNext()) {
			Object element = itr.next();
			System.out.println(element + "\n");

		}
		System.out.println("##############*************##################"
				+ "\n");
	}

	public void test_Company() {
		ArrayList<Company> CPNlist = new ArrayList<Company>();
		CPNlist.add(new Company("Emquatier", 1800000, 500000));
		CPNlist.add(new Company("Paragon", 6000000, 300000));
		CPNlist.add(new Company("Termina 21", 1750000, 1700000));

		Collections.sort(CPNlist, new EarningComparator());
		Iterator itr = CPNlist.iterator();
		System.out.println("##########Sort By Income##########");
		while (itr.hasNext()) {
			Object element = itr.next();
			System.out.println(element + "\n");

		}
		Collections.sort(CPNlist, new ExpenseComparator());
		Iterator itr2 = CPNlist.iterator();
		System.out.println("##########Sort By Expense##########");
		while (itr2.hasNext()) {
			Object element = itr2.next();
			System.out.println(element + "\n");
		}
		
		Collections.sort(CPNlist, new ProfitComparator());
		Iterator itr3 = CPNlist.iterator();
		System.out.println("##########Sort By Profit##########");
		while (itr3.hasNext()) {
			Object element = itr3.next();
			System.out.println(element + "\n");
		}
	}
}
