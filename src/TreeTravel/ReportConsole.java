package TreeTravel;

public class ReportConsole {
	
	public void display(Node root,Traversal traversal){
		System.out.println("Traverse with "+traversal+" : "+traversal.traverse(root));
	}
}
