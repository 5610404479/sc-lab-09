package TreeTravel;
import java.util.ArrayList;
import java.util.Stack;


public class InOrderTraversal implements Traversal{

	@Override
	public ArrayList<Node> traverse(Node node) {
		ArrayList<Node> ans = new ArrayList<Node>();
		Stack<Node> stack = new Stack<Node>();
		Node current = node;

		while (!stack.isEmpty() || current != null){
			if(current != null){
				stack.push(current);
				current = current.getLeft();
			}
			else{
				current = stack.pop();
				ans.add(current);
				current = current.getRight();
			}
		}
		
		return ans;
	}
	
	public String toString(){
		return "InOrderTraversal";
	}
}
