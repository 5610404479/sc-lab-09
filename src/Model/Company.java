package Model;

import Interface.Measurable;
import Interface.Taxable;

public class Company implements Taxable,Measurable {
	private String name;
	private double income;
	private double outcome;
	private double profit;

	public Company(String name, double income, double outcome) {
		this.name = name;
		this.income = income;
		this.outcome = outcome;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getIncome() {
		return income;
	}

	public void setIncome(double income) {
		this.income = income;
	}

	public double getOutcome() {
		return outcome;
	}

	public void setOutcome(double outcome) {
		this.outcome = outcome;
	}

	@Override
	public double getMeasure() {
		// TODO Auto-generated method stub
		profit = this.income - this.outcome;
		return profit ;
	}

	@Override
	public double getTax() {
		double sumTax = 0;
		sumTax = (this.income - this.outcome) * 0.3;
		return sumTax;
	}

	@Override
	public String toString() {
		return "Company [name=" + name + ", income=" + income + ", outcome="
				+ outcome + ", profit=" + profit + "]";
	}

	

}