package Model;

import Interface.Taxable;

public class Product implements Taxable, Comparable {
	private String name;
	private double price;

	public Product(String name, double price) {
		this.name = name;
		this.price = price;
	}

	public String getName() {
		return this.name;
	}

	public double getPrice() {
		return this.price;
	}

	

	@Override
	public int compareTo(Object obj) {
		if (this.price == ((Product) obj).getPrice())
			return 0;
		else if ((this.price) > ((Product) obj).getPrice())
			return 1;
		else {
			return -1;
		}
	}


	@Override
	public String toString() {
		return "Product [name=" + name + ", price=" + price + "]";
	}

	@Override
	public double getTax() {
		double sumTax = 0;
		sumTax = this.price * 0.07;

		return sumTax;
	}
}
