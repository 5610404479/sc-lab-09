package Model;

import Interface.Measurable;

public class Country implements Measurable {
	@Override
	public String toString() {
		return "Country [name=" + name + ", population=" + population + "]";
	}

	private String name;
	private int population;

	public Country(String name, int population) {
		this.name = name;
		this.population = population;
	}

	public String getName() {
		return this.name;
	}

	public int getPopulation() {
		return this.population;
	}

	@Override
	public double getMeasure() {
		// TODO Auto-generated method stub
		return this.getPopulation();
	}

}
